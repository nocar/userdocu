Acoustic Wave Propagration of a Cylindrical Wave
================================================

Problem Definition
--------------------

This example shows acoustic wave propagation in a transient analysis in 2D.
Waves are excited by a sin-burst signal on a circular cylinder.
They propagate outwards into a 1/4 domain (2 symmetries have been exploited).

```
Sketch of the domain

    _________________________
   |*************************|
   |*************************|
   |********** air **********|
   |*************************|
   |*************************|
^  ...***********************|
|  .......*******************|
R  .........*****************|
|  ..........****************|
v  ...........***************|
               ‾‾‾‾‾‾‾‾‾‾‾‾‾‾
    <----R--->


```

![Sketch of the domain](Sketch-acou.png)


Meshing
------------

Workflow (see the file [`run.sh`](run.sh)):

1. Mesh the domain, see Trelis file [`domain.jou`](domain.jou)
2. The journal file [`domain.jou`](domain.jou) from (1.) is used as an input.
3. An ANSYS-cdb mesh file [`domain.cdb`](domain.cdb) is created.
4. You can also save the created geometry as `domain.trelis` to open it directly on *Trelis* (optional).
5. Look into the journal file [`domain.jou`](domain.jou) to see the Trelis commands to create the mesh.
6. These commands could also have been run interactively in Trelis.

The file [`domain.cdb`](domain.cdb) was created this way.


## Simulation with CFS

Use an XML-editor (e.g. *oXygen* or *eclipse*) to define the simulation input for CFS.

For all the cases, we have the following generic procedure: 

The input file ([`transient.xml`](transient.xml)) is the simulation input.
In the file ([`mat_acou.xml`](mat_acou.xml)) the material porperties are defined.

To start the computation run the following command in the terminal
```shell
cfs -p transient.xml job
```
where `job` can be any name you choose for the simulation.

CFS will write some output on the terminal, and produce the two files
* `job.info.xml`, which contains some details about the run, and
* `job.cfs` in the `results_hdf5` directory, which you can view with ParaView.



Postprocessing
--------------

1. collect all sensor-array files to one by using [`combineSensorArray.py`](combineSensorArray.py)
2. look at the field result in ParaView (load pipeline from [`post.pvsm`](post.pvsm))


Further Suggestions
--------------------

Think about the following questions:

* Time of flight: how much time does the signal take from the excitation to a microphone location in the center of the domain?
* Amplitude decay: A wave with a pressure amplitude of 1 is excited, why is the amplitude in the center of the domain smaller?
* What happens when the wave reaches the boundary of the domain?
  Apply absorbing boundary conditions (ABC) and compare the results.
* How coarse can the mesh be?
  How large can the timestep get?
  Conduct a convergence study ...
