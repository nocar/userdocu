# Non-Conforming Interfaces

Non-conforming interfaces allow the usage of non-matching, interacting grids connected by an interface. In the following explanation the Nitsche method [@Kaltenbacher2010] is presented, because it more robust in practical applications [@huppe2014]. For a detailed deriviation look into [@Kaltenbacher2010] and [@huppe2014].

![a](ncinterfaces.png)

Consequently, non-conforming interfaces allow to use different meshes in the same simulation, as shown in the above figure. Both domains, $\Omega_1$ and $\Omega_2$ exist of different grids and are connected by the non-conform interface $\Gamma_I$. This is especially interesting if different materials are taken into account while a coupled simulation is performed. Furthermore, they are used for simulate rotating structures (for more details look into Features -> Rotating Interfaces) [@schoder2020b].

For defining a non-conform interface first the surfaces, that are part of the interface, have to be defined

```
  <domain geometryType="plane">
    <surfRegionList>
      <surfRegion name="innerR" />
      <surfRegion name="outerR" />
    </surfRegionList>
  </domain>
```

In a second step the actual interface has to be established


```
  <domain geometryType="plane">
    <surfRegionList>
      <surfRegion name="innerR" />
      <surfRegion name="outerR" />
    </surfRegionList>
    
    <ncInterfaceList>
      <ncInterface name="ncR" masterSide="outerR" slaveSide="innerR"/>
    <ncInterfaceList>    
  </domain>
```

Those two steps establish the interface of the domain. Now, we have to establish the formulation of the interface

```
    <pdeList>
      <acoustic formulation="acouPressure">
        <ncInterfaceList>
          <ncInterface name="ncR" nitscheFactor="100" formulation="Nitsche"/>
        </ncInterfaceList>      
      </acoustic>
    </pdeList>
```

The nitscheFactor is only avaiable for non-conforming interfaces type Nitsche and defines the weighting of the coupling of the two domains $\Omega_1$ and $\Omega_2$. This factor has no major impact in the range of 20 - 100 [@huppe2014]. A rather high value correlates with a strong coupling, with from a physical point of few is good, but makes the interface numerically instable. On the other hand, a low nitscheFactor is numerically more stable, but can make the coupling unphysical. 

(TODO mortar NC interface)
