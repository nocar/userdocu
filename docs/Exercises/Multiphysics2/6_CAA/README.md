# HW6: Aeroacoustics


### Description
This problem considers a 2D cylindrical obstacle (diameter $d$=0.02 m) within an incompressible flow.
Inflow velocity $U_0$ = 10 m/s, kinematic viscosity $\nu$ = 0.001 m$^2$ /s, Reynolds number $Re \approx$ 200.

Air at 20$^\circ$C should be defined as material for the simulation (Material parameters are inside the `air.xml` file.). The computational aeroacoustic domain is depicted below.

The CFD and acoustic source interpolation computations have already been performed (see `acousticSources.h5`). We solve the Perturbed Convective Wave Equation (PCWE) in the frequency domain assuming low Mach number ($Ma <$ 0.3). The convective wave equation fully describes acoustic sound generated by incompressible flow structures and its wave propagation.
The only unkown is the acoustic scalar potential $\psi^{\rm a}$ . In order to receive the acoustic pressure, we have to derive the scalar potential and scale it with the density.

![sketch1](1.png){: style="width:500px"}

---------------------------

### Tasks
1. Answer the theoretical questions. **(4 Points)**
2. Create input-xml files for the required CFS++ simulations to perform a harmonic analysis of the provided source field (acousticSources.h5). **(3 Points)**
3. Document the results you got from the CFS++ simulation by answering the questions below. **(8 Points)**
	- Plot the calculated SPL in dB vs. frequency in Hz (for `mic1` and `mic2`). You can use the resulting `.hist` files. 
	- Add a directivity plot, radius 2 m, frequency 97Hz. You can use the resulting files in the folder mics.
	- Discuss the results

### Theoretical part
- Define the Strouhal number and calculate the Mach number. What is the expected shedding frequency? Assume standard ambient conditions.
- Describe Lightshill’s idea towards his aeroacoustic analogy?
- Describe the main difference between PCWE and Lighthill's analogy. 

---------------------------

### Hints
- Use the provided templates [`acoustic_ue.xml`](acoustic_ue.xml) and [`air.xml`](air.xml)
- Use the provided `.cfs`-files: [`PML.cfs`](PML.cfs) and [`propagationRegion.cfs`](propagationRegion.cfs)
- The evaluation of microphone positions defined in [`MicPos.txt`](MicPos.txt) can be found in [`Mic_Auswertung.py`](Mic_Auswertung.py)
- The acoustic source data is given in [`acousticSources.h5`](acousticSources.h5)
- Use the provided file [`hist.py`](hist.py).
- State the reference pressure if something is in dB.

### Submission
Submit all your input files (`*.jou`, `*.xml`) as well as a concise PDF report of your results. Name the archive according to the format `HWn_eNNNNNNN_Lastname.zip`.

### Setup
- Include material file (`air.xml`)
- Assign material to regions (air 20 degrees)
- Define interfaces between the different regions/parts (Nitsche-type Mortaring)
- Define microphone points for evaluation 

---------------------------

You can download the templates linked on this page individually by _right click --> save target as_.
Alternativey, head over to the [git repository](https://gitlab.com/openCFS/userdocu/-/tree/master/docs/Exercises/Multiphysics2) and use the _download button_ on the top right: 
Here is a direct link to the [__zip archive__](https://gitlab.com/openCFS/userdocu/-/archive/master/HW6.zip?path=docs/Exercises/Multiphysics2/6_MagMech).
