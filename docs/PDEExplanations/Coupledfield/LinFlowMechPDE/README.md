## LinFlow-Mechanic coupling
openCFS provides the coupling between incompressible/compressible viscous fluid with flexible structure by using the Nitsche method.
### Coupling conditions
In LinFlow-mechanic coupled problems, two transmission conditions along the interfaces have to be fulfilled:

* **Dynamic coupling condition** is the continuity of traction (force equilibrium) at the interface 

\begin{equation}
\mathbf \sigma_\mathrm{m} \cdot \mathbf n = \mathbf \sigma_\mathrm{v} \cdot \mathbf n \, ,
\label{eq:tractranscond-mechcoupl}
\end{equation}

$\mathbf \sigma_\mathrm{m} $ and $\mathbf \sigma_\mathrm{v}$ are mechanic and LinFlow stress tensors, respectively and the surface normal vector is $\mathbf n = \mathbf {n}_{\rm m} = -\mathbf{n}_{\rm v}$

* **Kinematic coupling condition** is velocity continuity at the interface
\begin{equation}
\frac{\partial \mathbf u}{\partial t} = \mathbf v \, ,
\label{eq:veltranscond-mechcoupl}
\end{equation}
where $\mathbf u$ and $\mathbf v$ are mechanic displacement and LinFlow velocity vector, respectively.

### Weak formulation
To obtain the coupling PDEs in Nitsche method, the weak form of [mechanic PDE](../../Singlefield/MechanicPDE) and balance of momentum in [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md) will be combined and the traction continuity condition (\ref{eq:tractranscond-mechcoupl}) will be applied

\begin{equation}
\int\limits_{\Omega_\mathrm{v}} \rho_0 \mathbf v' \cdot \frac{\partial \mathbf v}{\partial t}\, {\mathrm {d}} \Omega +
\int\limits_{\Omega_\mathrm{v}} \nabla \mathbf v' : \mathbf \sigma_\mathrm{v} {\mathrm {d}}\Omega +
\int\limits_{\Omega_\mathrm{m}} \mathbf u’ \cdot \rho_\mathrm{m} \frac{\partial^2 \mathbf u}{\partial t^2} {\mathrm {d}} \Omega+
\int\limits_{\Omega_\mathrm{m}} \nabla \mathbf u’ : \mathbf \sigma_\mathrm{m} {\mathrm {d}} \Omega -
\int\limits_{\Gamma_\mathrm{i}} (\mathbf u’ - \mathbf v') \cdot \mathbf \sigma_\mathrm{v} \cdot \mathbf n {\mathrm {d}} \Gamma = \mathbf 0.
\label{eq:mechcoupl1}
\end{equation} 

In this equation, $\mathbf{v'}$, $\mathbf{u’}$ are fluid velocity test functions,
mechanical displacement test functions, $\rho_0$ and $\rho_\mathrm{m}$ are LinFlow viscous fluid and mechanic density, respectively.
The final version of Nitsche coupling formulations will be obtained by adding the penalty term

\begin{eqnarray}
\label{eq:NitscheWeak}
\int\limits_{\Omega_\mathrm{v}} \rho_0 \mathbf v' \cdot \frac{\partial \mathbf v}{\partial t}\, {\mathrm {d}} \Omega +
\int\limits_{\Omega_\mathrm{v}} \nabla \mathbf v' : \mathbf \sigma_\mathrm{v} {\mathrm {d}}\Omega +
\int\limits_{\Omega_\mathrm{m}} \mathbf u’ \cdot \rho_\mathrm{m} \frac{\partial^2 \mathbf u}{\partial t^2} {\mathrm {d}} \Omega+
\int\limits_{\Omega_\mathrm{m}} \nabla \mathbf u’ : \mathbf \sigma_\mathrm{m} {\mathrm {d}} \Omega \nonumber \\ -
\underbrace{\int\limits_{\Gamma_\mathrm{i}} (\mathbf u’ - \mathbf v') \cdot \mathbf \sigma_\mathrm{m} \cdot \mathbf n {\mathrm {d}} \Gamma}_{{Traction}}+
\underbrace{\beta \sum_\mathrm{e} \frac{1}{h_\mathrm{ e}}\int\limits_{\Gamma_\mathrm{i}^e}(\mathbf u’ -\mathbf v')\cdot (\frac{\partial \mathbf u}{\partial t}- \mathbf v) {\mathrm {d}} \Gamma }_{{Penalty}}= 0
\end{eqnarray}


In this formulation, the *penalty* term guarantees the velocity continuity, and the term *traction* guarantees the traction continuity at the interface.

The penalty term is considered dependent on the characteristic length $h_{\rm e}$ (which is mesh-dependent). 
This characteristic length is the smaller element length between LinFlow or mechanic regions $h_{\rm e} = \operatorname{min}(h_{\rm v}, h_{\rm m})$.
 
The penalty factor $\beta$ is defined as

\begin{eqnarray}
\beta =\operatorname{max}(p_{\rm m},p_{\rm v})^2 \sqrt[]{\frac{ \rho_0}{\rho_{\rm m}}} (\mu + \lambda)\beta_0.
\label{eq:penaltyfac}
\end{eqnarray}

Where $\beta_0$ is Nitsche factor and $p_{\rm m}$ and $p_{\rm v}$ are the order of basic functions for mechanic and LinFlow fluid domains, respectively.

Assigning the correct value of the Nitsche factor $\beta_0$ is crucial for solving this formulation: 
A small value of the Nitsche factor does not meet velocity continuity at the interface. 
In contrast, a high value of the Nitsche factor makes numerical errors.
A Nitsche factor between $10^7-10^{14}$ is appropriate for LinFlow-mechanic coupling problems.



### Analysis Types
Same as [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md) the coupling can be solved in harmonic transient studies.

* **Harmonic**

*not validated yet:* **Transient** 


### Defining the coupling
LinFlow-mechanics coupling benefits form [non-conforming](../../../Tutorials/Features/ncinterfaces.md) interfaces option.
Therefore, similar to other non-conforming interface couplings, in simulation xml-file the non-conforming interface option needs to be defined as

```
    <domain geometryType=""> 
        <regionList>
            <region name="" material=""/>
        </regionList> 
        <surfRegionList>
            <surfRegion name="LinFLow"/>
            <surfRegion name="Mechanic"/>
        </surfRegionList>        
        <ncInterfaceList>
            <ncInterface name="LinFLow_mech_interface" masterSide="Mechanic" slaveSide="LinFLow"/>
        </ncInterfaceList>
    </domain>
```
Note that in this coupling, the LinFlow interface is considered as the ```  slaveSide``` and the mechanic interface is considered as the ```masterSide```.
In the direct coupling section the non-conforming interface defined in ``` <ncInterfaceList> ``` will be entered as

```
        <couplingList>
            <direct>
                
                <linFlowMechDirect>
                    <ncInterfaceList>
                        <ncInterface name="LinFLow_mech_interface" formulation="Nitsche" nitscheFactor="10e10"/>
                    </ncInterfaceList>
                </linFlowMechDirect>
                
            </direct>
        </couplingList>
```
In this coupling, defining ``` nitscheFactor``` is optional and by default is set to $10^{10}$.
### Material and postprocessing results

In openCFS material-xml file beside the flow material input for the [LinFlow PDE](../../Singlefield/LinFlowPDE/README.md), a mechanic material input for the [mechanic PDE](../../Singlefield/AcousticPDE) should also be defined.
```
  <material name="Air">
    <flow>
        ...
    </flow>
  </material>

  <material name="Silicon">
    <mechanical>
        ....
    </mechanical>
  </material>
```
The postprocessing results and the boundary conditions for each PDE are described in the "Singlefield" section. 

