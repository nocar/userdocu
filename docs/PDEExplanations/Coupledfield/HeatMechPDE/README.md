## Heat conduction - mechanics coupling
This coupling is modelled as a pure forward coupling from a heat conduction simulation, e.g. performed in sequence step 1, to a computational mechanics using the temperature distribution and consider the thermal stress as a right hand side load.

### Coupling conditions
The thermal stain $\mathbf{s}_\mathrm{th}$ is model as

\begin{equation}
\mathbf{s}_\mathrm{th} = \mathbf{\alpha}_\mathrm{th} \Delta T = \mathbf{\alpha}_\mathrm{th}
 \big( T - T_\mathrm{ref} \big)
\end{equation}

with a given reference temperatur $T_\mathrm{ref}$ and the thermal expansion tensor of rank 2

\begin{equation}
\mathbf{s}_\mathrm{th} = \left(
\begin{array}{lll}
\alpha_{11} & \alpha_{12} & \alpha_{13} \\
            & \alpha_{22} & \alpha_{23} \\
\text{sym}  &             & \alpha_{33}
\end{array} \right)
\end{equation}

In doing so, the [mechanical PDE](../../Singlefield/MechanicPDE/README.md) for the displacement $\mathbf{u}$ changes to
 
\begin{equation}
\rho \frac{\partial^2 \mathbf{u}}{\partial t^2} - \mathbf{\mathcal{B}}^T \mathbf{C}
\mathbf{\mathcal{B}} \mathbf{u} = \mathbf{g} +  \mathbf{\mathcal{B}}^T \mathbf{C}
\mathbf{\alpha}_\mathrm{th} 
\end{equation}



### Analysis Types

Depending on the temporal setting, the coupled PDEs can be solved for the following analysis types:

* **Static case** ($\partial / \partial t = 0$)
```
<analysis>
	<static>
	</static>
</analysis>
```
* **Transient case** ($\partial / \partial t (\cdot) \neq 0$) 
```
<analysis>
  <transient>
		<numSteps>100</numSteps>
		<deltaT>1e-3</deltaT>
  </transient>
</analysis>
```

### Single PDEs

As described, a typical application is the computation of a temperature distribution in sequence step 1 and then in step 2 the deformed mechanical structure, e.g

```
    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="S_beam"/>
                </regionList>
                <bcsAndLoads>
                   <temperature name="L_top" value="30"/>
                   <temperature name="L_bottom" value="20"/>
                </bcsAndLoads>
                ....               
            </heatConduction>
        </pdeList>
    </sequenceStep>
     
    <sequenceStep index="2">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <mechanic subType="planeStress">
                <regionList>
                    <region name="S_beam"/>
                </regionList>
                <bcsAndLoads>
                    <fix name="L_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                    </fix>
                    <thermalStrain name="S_beam">
                        <sequenceStep index="1">
                            <quantity name="heatTemperature" pdeName="heatConduction"/>
                            <timeFreqMapping>
                                <constant step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </thermalStrain>
                </bcsAndLoads>
                ....
            </mechanic>
        </pdeList>
    </sequenceStep>
```


### Defining the coupling
* **Coupling terms:** Within the mechanical PDE we assume to have on the computational domain a temperature distribution, e.g. from a previous sequence step (see above), or which is provided by the input file and then is defined by

```
 <bcsAndLoads>
      ...    
      <thermalStrain name="Vol_bpsg_B1">
           <grid>
                <defaultGrid quantity="heatTemperature" dependtype="GENERAL" sequenceStep="2">
                      <globalFactor>1</globalFactor>
                </defaultGrid>
           </grid>
      </thermalStrain>    
     ...
 </bcsAndLoads>                
```
 
### Material and postprocessing results

The thermal expansion tensor of rank 2 is defined within the material file, e.g. for an isotropic material as follows
```
<material name="alu">
      <mechanical>
          <density>2700.0</density>
          ...
          
          <thermalExpanison>
              <isotropic>
                  <real>22e-6</real>
              </isotropic>
              <refTemperature>
                  <real>25</real>
              </refTemperature>
          </thermalExpanison>
          ...
      </mechanical>    
```
Additional posprocessing results available for the mechaical PFE are defined in ```<storeResults>```

```
          <storeResults>
                ...
                <elemResult type="mechThermalStress">                   
                      <allRegions/>
                </elemResult>
                <elemResult type="mechThermalStrain">                   
                      <allRegions/>
                </elemResult>
                ...
           </storeResults>
```


