# Volume Multiplication

This filter allows to multiply values given on elements by the elements volume ATTENTION: This does not work for values given on nodes!


```
    <volumeMultiplication id="..." inputFilterIds="...">
      <singleResult>
        <inputQuantity resultName="..."/>
        <outputQuantity resultName="..."/>
      </singleResult>
    </volumeMultiplication>
```
